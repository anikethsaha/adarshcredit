<?php

//require user configuration and database connection parameters
require('config.php');
require './csrf_token.php';
$_GET['status'] = "";
$_GET['msg'] = "";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link
      rel="stylesheet"
      href="https://bootswatch.com/4/journal/bootstrap.min.css"
    />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />
    <title>Register</title>

    <script src='https://www.google.com/recaptcha/api.js?render=<?php echo  $sitekey; ?>'></script>
  </head>
  <body>

    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
      crossorigin="anonymous"
    ></script>

    <div class="row mt-5">
  <div class="col-md-6 m-auto">
    <div class="card card-body">
      <h1 class="text-center mb-3">
        <i class="fas fa-user-plus"></i> Register
      </h1>



      <form  id="reg_form" action="<?php echo htmlentities('./register.php'); ?>" method="POST" >

      <input required  type="hidden" name="_token" class="_token" value="<?php echo $token ?>" >

        <div class="form-group">
          <label for="name">Name</label>
          <input
          required
            type="name"
            id="name"
            name="desired_username"
            class="form-control"
            placeholder="Enter Name"

          />
        </div>
        <div class="form-group">
          <label for="email">Email  </label>
          <input
          required
            type="email"
            id="email"
            name="desired_email"
            class="form-control"
            placeholder="Enter Email"

          />
        </div>
        <div class="form-group">

          <input
          required
            type="hidden"
            id="g-recaptcha-response"
            name="g-recaptcha-response"
            class="form-control"

          />
        </div>


        <button type="submit" value="register" class="btn btn-primary btn-block">
          Register
        </button>
      </form>
      <p class="lead mt-4">Have An Account? <a href="/users/login">Login</a></p>
    </div>
  </div>
</div>
        <!-- End of registration form -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.12.0/validate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script>
grecaptcha.ready(function() {
grecaptcha.execute('<?php echo  $sitekey; ?>', {action: 'action_name'})
.then(function(token) {
// Verify the token on the server.
    document.getElementById('g-recaptcha-response').value = token
});
});
</script>

<script>

$('#reg_form').on('submit',(e) => {
  $('#reg_form button').html("Loading....")
  e.preventDefault();
    var desired_username = $("#reg_form input[name='desired_username']").val();
    var desired_email = $("#reg_form input[name='desired_email']").val();

      var university = $("#reg_form input[name='university']");
      var grecaptcharesponse = $('#g-recaptcha-response').val();


    if(validate.single(desired_email, {presence: true, email: true}) !== undefined){
      toastr.warning("ERROR",validate.single(desired_email, {presence: true, email: true}))
      return 0;
    }


    desired_username = validate.prettify(desired_username)


    if( !validate.isString(desired_username)){
      toastr.warning("ERROR","User Invalid")
      return 0;

    }
    $.ajax({
        url:"<?php echo htmlentities('./register.php'); ?>",
        type:"post",
        dataType:"text",
        data : {
         desired_email,
         desired_username,
         "g-recaptcha-response" : grecaptcharesponse
        },
        headers: {
         'X-CSRF-TOKEN': $("#reg_form input[name='_token']").val()
        },
        success : async (res) => {
          console.log(res);
          toastr.info("Status",res)

          $('#reg_form button').html("Submit")
          location.reload();
        }

    })


  // filtering the inputs


});

</script>

</body>
</html>