
<?php

ini_set('session.cookie_lifetime', 60 * 60 * 24 * 7);
ini_set('session.gc_maxlifetime', 60 * 60 * 24 * 7);
session_start();
session_regenerate_id();

//Define MySQL database parameters
$username = "root";
$password = "";
$hostname = "localhost";
$database = "adarshCredit";
//Define your canonical domain including trailing slash!, example:
$domain = "http://localhost/adarshcreditAdmin";
//Define sending email notification to webmaster
$email = 'youremail@example.com';
$subject = 'New user registration notification';
$from = 'From: www.example.com';
//Define Recaptcha parameters
$secret  = "6LfhhYkUAAAAAOyxrNYffWTQ2WBu_PgEsfIfaNb-";
define('recaptcha_secret',"6LfhhYkUAAAAAOyxrNYffWTQ2WBu_PgEsfIfaNb-");
$publickey = "Your Recaptcha public key";
$sitekey = "6LfhhYkUAAAAAE3kZ5ZHAL3HE0dFdc4sNOaZ5NPo";
define('recaptcha_sitekey',"6LfhhYkUAAAAAE3kZ5ZHAL3HE0dFdc4sNOaZ5NPo-");
//Define length of salt,minimum=10, maximum=35
$length_salt = 15;
$token_length = 32;


//Define the maximum number of failed attempts to ban brute force attackers
//minimum is 5
$maxfailedattempt = 5;
//Define session timeout in seconds
//minimum 60 (for one minute)
$sessiontimeout = 180;
////////////////////////////////////
//END OF USER CONFIGURATION/////////
////////////////////////////////////
//DO NOT EDIT ANYTHING BELOW!
$conn = mysqli_connect($hostname, $username, $password,$database)
    or die("Unable to connect to MySQL");

$loginpage_url = $domain . '/auth/show_login.php';
$forbidden_url = $domain . '/auth/403forbidden.php';
?>