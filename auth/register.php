
<?php

require './utils/validator.php';
require './utils/generateToken.php';

require 'utils/password.php';
require 'config.php';

// function get_client_ip() {
//     $ipaddress = '';
//     if (getenv('HTTP_CLIENT_IP'))
//         $ipaddress = getenv('HTTP_CLIENT_IP');
//     else if(getenv('HTTP_X_FORWARDED_FOR'))
//         $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
//     else if(getenv('HTTP_X_FORWARDED'))
//         $ipaddress = getenv('HTTP_X_FORWARDED');
//     else if(getenv('HTTP_FORWARDED_FOR'))
//         $ipaddress = getenv('HTTP_FORWARDED_FOR');
//     else if(getenv('HTTP_FORWARDED'))
//        $ipaddress = getenv('HTTP_FORWARDED');
//     else if(getenv('REMOTE_ADDR'))
//         $ipaddress = getenv('REMOTE_ADDR');
//     else
//         $ipaddress = 'UNKNOWN';
//     return (string) $ipaddress;
// }


function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else

    if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];

    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}
$headers = apache_request_headers();

if ((isset($_POST["desired_username"])) && (isset($_POST["desired_email"]))
) {
    if(!isset($_COOKIE['CSRF_TOKEN']) || !isset($headers['X-CSRF-TOKEN'])) {
        die("Forbidden");
    }
    else{
        if( $_COOKIE['CSRF_TOKEN'] == $headers['X-CSRF-TOKEN'] ){
        include('./recaptchav3.php');
        $captchaObj = new Catpcha();
        $captchaResponse = $captchaObj->getCaptcha($_POST['g-recaptcha-response']);
        if($captchaResponse->success != true && $captchaResponse->score <= 0.5){
            echo "Invalid Captcha.....Please Try Again";
            die();
        }else{


        function sanitize( $conn,$data) {
            $data = trim($data);
            $data = htmlspecialchars($data);
            $data = mysqli_real_escape_string($conn,$data);
            return $data;
        }
        $desired_username = sanitize($conn,$_POST["desired_username"]);
        $desired_email = sanitize($conn,$_POST["desired_email"]);

        $validator = new FormValidator();

        $validator->validateItem($desired_email, 'email');
        $validator->validateItem($desired_username, 'string');

        $desired_email = $validator->sanatizeItem($desired_email, 'email');
        $desired_username = $validator->sanatizeItem($desired_username, 'string');

                $stmt = $conn->prepare(" SELECT username FROM users WHERE email = ? ");
                $stmt->bind_param("s", $desired_email);
                $stmt->execute();
                $stmt->store_result();
                $stmt->fetch();

                if($stmt->num_rows > 0){

                    echo "Username Or Email Already Been Used.....Please Try Again";
                    $stmt->close();
                    die();
                    // header('Location: /auth/show_register.php?status="FAILED"&msg="UserName Already Taken"');
                }else{
                    $stmt->close();
                    $optionsForPassHash = [
                        // 'salt' => base64_encode("anyEncryptedRandomString__")
                        'cost' => 11
                    ];
                    $generator = new RandomStringGenerator();
                    $randomPassword = $generator->generate($token_length);
                    $hashpassword = password_hash($randomPassword, PASSWORD_BCRYPT);

                    $sql = "
                    INSERT INTO users ( username, email, password,temp_password ,isActive,ip ,created_on,role_group)
                    VALUES ('".$desired_username."', '".$desired_email."','".$hashpassword."', '".$randomPassword."' ,FALSE, '".$_SERVER['REMOTE_ADDR']."', '".date('m/d/Y h:i:s a', time())."' , 2)";

                    // $sql = "INSERT INTO users ( username, email, password, isActive ,created_on,role_group)
                    // VALUES (?,?,?,?,?,?)";

                    // $insertstmt = $conn->prepare($sql);
                    // $insertstmt->bind_param("ssssss",
                    //     $desired_username,
                    //     $desired_email,
                    //     $hashpassword,
                    //     FALSE,
                    //     date('m/d/Y h:i:s a', time()),
                    //     2);

                    // $result = $insertstmt->execute();


                    if(mysqli_query($conn, $sql) /* $result */){
                        echo "<h5>Successfull....Please check your mail, soon we will send you link to grant access to our system</h5>";
                        // header('Location: /auth/show_register.php?status="SUCCESS"&msg="Registration Success"');
                        $conn->close();
                    }else{
                        // echo mysqli_error($conn);
                        echo "FAILED.....Please try Again".mysqli_error($conn);
                        $conn->close();
                        // header('Location: /auth/show_register.php?status="FAILED"&msg="'.mysqli_error($conn).'"');

                    }
                }


        }
    }else{
        die("forbidden");
    }

    }
    exit;
}

?>
