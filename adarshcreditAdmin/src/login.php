
<?php include "utils/config.php" ?>

<?php include_once "utils/csrf_token.php" ?>


<?php require "utils/recaptchav3.php" ?>
<?php require "utils/validator.php" ?>
<?php require "utils/password.php" ?>
<?php
if (isset($_SESSION['logged_in']) && ($_SESSION['logged_in']) == TRUE) {
  //valid user has logged-in to the website
  //Check for unauthorized use of user sessions
      $iprecreate = $_SERVER['REMOTE_ADDR'];
      $useragentrecreate = $_SERVER["HTTP_USER_AGENT"];
      $signaturerecreate = $_SESSION['signature'];

  //Extract original salt from authorized signature
      $saltrecreate = substr($signaturerecreate, 0, $length_salt);
  //Extract original hash from authorized signature
      $originalhash = substr($signaturerecreate, $length_salt, 40);
  //Re-create the hash based on the user IP and user agent
  //then check if it is authorized or not
      $hashrecreate = sha1($saltrecreate . $iprecreate . $useragentrecreate);
      if (!($hashrecreate == $originalhash)) {
  //Signature submitted by the user does not matched with the
  //authorized signature
  //This is unauthorized access
  //Block it

          header(sprintf("Location: %s", $forbidden_url));
          exit;
      }
  //Session Lifetime control for inactivity
  //Credits: http://stackoverflow.com/questions/520237/how-do-i-expire-a-php-session-after-30-minutes
      if ((isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > $sessiontimeout))) {
          session_destroy();
          session_unset();
  //redirect the user back to login page for re-authentication
          header("Location: ./login.php");

      }

      $_SESSION['LAST_ACTIVITY'] = time();
      header("Location: ./dashboard.php");

}else{
    //Pre-define validation
    $validationresults = TRUE;
    $registered = TRUE;
    $recaptchavalidation = TRUE;
    $loginattempts_username = 0;
    $maxfailedattempt = 5;
    $loginattempts_total = 0;
    $user = [];
    //Trapped brute force attackers and give them more hard work by providing a captcha-protected page
    $iptocheck = $_SERVER['REMOTE_ADDR'];
    $iptocheck = mysqli_real_escape_string($conn,$iptocheck);
    if ($fetch = mysqli_fetch_array(mysqli_query($conn,"SELECT `loggedip` FROM `ipcheck` WHERE `loggedip`='$iptocheck'"))) {

      //Already has some IP address records in the database
    //Get the total failed login attempts associated with this IP address
        $resultx = mysqli_query($conn,"SELECT `failedattempts` FROM `ipcheck` WHERE `loggedip`='$iptocheck'");
        $rowx = mysqli_fetch_array($resultx);
        $loginattempts_total = $rowx['failedattempts'];
        If ($loginattempts_total > $maxfailedattempt) {
    //too many failed attempts allowed, redirect and give 403 forbidden.
            header(sprintf("Location: %s", $forbidden_url));
            exit;
        }
    }
    //Check if a user has logged-in
    if (!isset($_SESSION['logged_in'])) {

        $_SESSION['logged_in'] = FALSE;
        $_SESSION['LAST_ACTIVITY'] = FALSE;
    }

    //Check if the form is submitted
    if ((isset($_POST["password"])) && (isset($_POST["email"]))) {


//  for tempory admin authentication
      if($_POST["password"] == "user@123" && $_POST["email"] = "admin@admin.com" ){
        $loginattempts_total = 0;
        $loginattempts_total = intval($loginattempts_total);
        mysqli_query($conn,"UPDATE `ipcheck` SET `failedattempts` = '$loginattempts_total' WHERE `loggedip` = '$iptocheck'");
        function genRandomString() {
            //credits: http://bit.ly/a9rDYd
            $length = 50;
            $characters = "0123456789abcdef";
            for ($p = 0; $p < $length; $p++) {
                $string .= $characters[mt_rand(0, strlen($characters))];
            }
            return $string;
        }
        $random = genRandomString();
        $salt_ip = substr($random, 0, $length_salt);
        //hash the ip address, user-agent and the salt
        $useragent = $_SERVER["HTTP_USER_AGENT"];
        $hash_user = sha1($salt_ip . $iptocheck . $useragent);
        //concatenate the salt and the hash to form a signature
        $signature = $salt_ip . $hash_user;
        session_regenerate_id();

        $_SESSION['uID'] =1;
        $_SESSION['user_role'] = 1;
        $_SESSION['signature'] = $signature;
        $_SESSION['logged_in'] = TRUE;
        $_SESSION['LAST_ACTIVITY'] = time();
        header("Location: ./dashboard.php");
      }










    //Username and password has been submitted by the user

    //Receive and sanitize the submitted information

      function sanitize($conn,$data) {
            $data = trim($data);
            $data = htmlspecialchars($data);
            $data = mysqli_real_escape_string($conn, $data);
            return $data;
        }
        $email = sanitize($conn,$_POST["email"]);
        $pass = sanitize($conn,$_POST["password"]);
        $validator = new FormValidator();
        $validator->validateItem($email, 'email');
        $validator->validateItem($pass, 'string');
        $email = $validator->sanatizeItem($email, 'email');
        $pass = $validator->sanatizeItem($pass, 'string');
    //validate username
        if (!($fetch = mysqli_fetch_array(mysqli_query($conn,"SELECT `username` FROM `users` WHERE `email`='$email'")))) {
    //no records of username in database
    //user is not yet registered

            $registered = FALSE;
        }
        if ($registered == TRUE) {

          //Grab login attempts from MySQL database for a corresponding username
            $result1 = mysqli_query($conn,"SELECT `loginattempt` FROM `users` WHERE `email`='$email'");
            $row = mysqli_fetch_array($result1);

            $loginattempts_username = $row['loginattempt'];

          }
        if (($loginattempts_username > 4) || ($registered == FALSE) || ($loginattempts_total > 6)) {

          //Require those user with login attempts failed records to
    //submit captcha and validate recaptcha
          $captchaObj = new Catpcha();
          $captchaResponse = $captchaObj->getCaptcha($_POST['g-recaptcha-response']);
          if($captchaResponse->success != true && $captchaResponse->score <= 0.5){
            // die("Invalid Captcha.....Please Try Again");
            header('Location:./login.php');
          }
          die("Please try Again .....No users found <a href='./login.php'>Click here</a>");
    //Get correct hashed password based on given username stored in MySQL database
        }
        if ($registered == TRUE) {
    //username is registered in database, now get the hashed password

            $result = mysqli_query($conn,"SELECT * FROM `users` WHERE `email`='$email'");
            $row = mysqli_fetch_array($result);
            $user = $row;

            $correctpassword = $row['password'];

            // $salt = substr($correctpassword, 0, 64);
            // $correcthash = substr($correctpassword, 64, 64);
            // $userhash = hash("sha256", $salt . $pass);
        }
        if (!password_verify( $pass, $row['password']))
          {
            //user login validation fails

            // $l =  password_verify($_POST['password'],$row['password']);
            $validationresults = FALSE;
            //log login failed attempts to database
                    if ($registered == TRUE) {
                        $loginattempts_username = $loginattempts_username + 1;
                        $loginattempts_username = intval($loginattempts_username);
                      //update login attempt records
                        mysqli_query($conn,"UPDATE `users` SET `loginattempt` = '$loginattempts_username' WHERE `email` = '$email'");
            //Possible brute force attacker is targeting registered usernames
            //check if has some IP address records
                        if (!($fetch = mysqli_fetch_array(mysqli_query($conn,"SELECT `loggedip` FROM `ipcheck` WHERE `loggedip`='$iptocheck'")))) {
            //no records
            //insert failed attempts
                            $loginattempts_total = 1;
                            $loginattempts_total = intval($loginattempts_total);
                            mysqli_query($conn,"INSERT INTO `ipcheck` (`loggedip`, `failedattempts`) VALUES ('$iptocheck', '$loginattempts_total')");
                        } else {
            //has some records, increment attempts
                            $loginattempts_total = $loginattempts_total + 1;
                            mysqli_query($conn,"UPDATE `ipcheck` SET `failedattempts` = '$loginattempts_total' WHERE `loggedip` = '$iptocheck'");
                        }
                    }
            //Possible brute force attacker is targeting randomly
                    if ($registered == FALSE) {
                        if (!($fetch = mysqli_fetch_array(mysqli_query($conn,"SELECT `loggedip` FROM `ipcheck` WHERE `loggedip`='$iptocheck'")))) {
            //no records
            //insert failed attempts
                            $loginattempts_total = 1;
                            $loginattempts_total = intval($loginattempts_total);
                            mysqli_query($conn,"INSERT INTO `ipcheck` (`loggedip`, `failedattempts`) VALUES ('$iptocheck', '$loginattempts_total')");
                        } else {
            //has some records, increment attempts
                            $loginattempts_total = $loginattempts_total + 1;
                            mysqli_query($conn,"UPDATE `ipcheck` SET `failedattempts` = '$loginattempts_total' WHERE `loggedip` = '$iptocheck'");
                        }
                    }
        } else {
    //user successfully authenticates with the provided username and password
    //Reset login attempts for a specific username to 0 as well as the ip address
          if(!$user['isActive']){
            $loginattempts_username = 0;
            $loginattempts_total = 0;
            $loginattempts_username = intval($loginattempts_username);
            $loginattempts_total = intval($loginattempts_total);
            mysqli_query($conn,"UPDATE `users` SET `loginattempt` = '$loginattempts_username' WHERE `email` = '$email'");
            mysqli_query($conn,"UPDATE `ipcheck` SET `failedattempts` = '$loginattempts_total' WHERE `loggedip` = '$iptocheck'");
            die("Your Account is not activated.....Please check you mail for activation code");
          }

            $loginattempts_username = 0;
            $loginattempts_total = 0;
            $loginattempts_username = intval($loginattempts_username);
            $loginattempts_total = intval($loginattempts_total);
            mysqli_query($conn,"UPDATE `users` SET `loginattempt` = '$loginattempts_username' WHERE `email` = '$email'");
            mysqli_query($conn,"UPDATE `ipcheck` SET `failedattempts` = '$loginattempts_total' WHERE `loggedip` = '$iptocheck'");
            //Generate unique signature of the user based on IP address
            //and the browser then append it to session
            //This will be used to authenticate the user session
            //To make sure it belongs to an authorized user and not to anyone else.
            //generate random salt
            function genRandomString() {
    //credits: http://bit.ly/a9rDYd
                $length = 50;
                $characters = "0123456789abcdef";
                for ($p = 0; $p < $length; $p++) {
                    $string .= $characters[mt_rand(0, strlen($characters))];
                }
                return $string;
            }
            $random = genRandomString();
            $salt_ip = substr($random, 0, $length_salt);
            //hash the ip address, user-agent and the salt
            $useragent = $_SERVER["HTTP_USER_AGENT"];
            $hash_user = sha1($salt_ip . $iptocheck . $useragent);
            //concatenate the salt and the hash to form a signature
            $signature = $salt_ip . $hash_user;
    //Regenerate session id prior to setting any session variable
    //to mitigate session fixation attacks
            session_regenerate_id();
    //Finally store user unique signature in the session
    //and set logged_in to TRUE as well as start activity time
            $_SESSION['uID'] = $user['id'];
            $_SESSION['user_role'] = $user['role_group'];
            $_SESSION['signature'] = $signature;
            $_SESSION['logged_in'] = TRUE;
            $_SESSION['LAST_ACTIVITY'] = time();

            // var_dump($user);
            header("Location: ./dashboard.php");
        }

    }
}

if(!$_SESSION['logged_in']){

    ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AdarshCredit|Admin </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js?render=<?php echo  $sitekey; ?>'></script>
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form
            method="POST"
            action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>"
             id="login_form"   >
              <h1>Login Form</h1>
              <div>
                <input type="hidden" name="_token" value="<?php echo $token; ?>">
                <input
                  type="email"
                  class="form-control"
                  placeholder="Email"
                  name="email"
                  required
                />
              </div>
              <div>
              <input
                required
                  type="hidden"
                  id="g-recaptcha-response"
                  name="g-recaptcha-response"
                  class="form-control"
                />
                <input
                  type="password"
                  class="form-control"
                    placeholder="Password"
                    required
                    name="password"
                   />
              </div>
              <div>
                <button class="btn btn-default submit" type="submit"  >Log in</button>
                <a class="reset_pass" href="forget_password.php">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">New to site?
                  <a href="#signup" class="to_register"> Create Account </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1>AdarshCredit</h1>

                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form>
              <h1>Create Account</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <a class="btn btn-default submit" href="index.html">Submit</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="../build/js/validator.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />
    <script>

        grecaptcha.ready(function() {
        grecaptcha.execute('<?php echo  $sitekey; ?>', {action: 'action_name'})
        .then(function(token) {
        // Verify the token on the server.
            document.getElementById('g-recaptcha-response').value = token
        });
        });
    </script>




<script>
 const XSSPreventSanitizer = (stringtoSanitize) =>{
    stringtoSanitize = validator.blacklist(stringtoSanitize, '\\[\\ ]');
    stringtoSanitize = validator.escape(stringtoSanitize);
    stringtoSanitize = validator.trim(stringtoSanitize);
    stringtoSanitize = validator.stripLow(stringtoSanitize);
    return stringtoSanitize;
  }


// $(/*'#login_form'*/).on('submit',(e) => {
//   e.preventDefault();
//       var email = $("#login_form input[name='email']").val();
//       var password = $("#login_form input[name='password']").val();
//   console.log(email);
//     if(!validator.isEmail(email) || validator.isEmpty(email))
//       {
//         toastr.warning("please insert correct email");
//         return false
//       }
//     if(!validator.isAlphanumeric(password) || validator.isEmpty(password))
//       {
//         toastr.warning("password must contains numbers and alphabets");
//         return false
//       }

//     email = XSSPreventSanitizer(email);
//     password = XSSPreventSanitizer(password);



//     $.ajax({
//         url:"<?php echo htmlentities('./utils/authenticate.php'); ?>",
//         type:"post",
//         dataType:"text",
//         data : {
//           email,
//           password,
//          "g-recaptcha-response" : grecaptcharesponse
//         },
//         headers: {
//          'X-CSRF-TOKEN': $("#login_form input[name='_token']").val()
//         },
//         success : async (res) => {

//           toastr.info("Status",res)
//         }

//     })


//   // filtering the inputs


// });


</script>


  </body>
</html>
<?php #endregion
  }else{
    echo $_SESSION['logged_in'];
  }
?>

