<?php


// for csrf attack protection I have created this csrf token manually as I am not using any framework
// but if using framework I would have used there prebuilt token --  --by Aniketh
// include "session_encryption.php";

 $options = [
    	'cost' => 10
    // 'salt' => base64_encode("anyEncryptedRandomString__") // I am using php 7.1.0 where salt option is removed
];
// $s_encrypt = new session_Ecnryption();
 $token =  password_hash(session_id(), PASSWORD_BCRYPT ,$options);

//  $token = $s_encrypt->encrypt("CSRF_TOKEN");

 $cookie_name = "CSRF_TOKEN";
 $expire = time() + (86400 * 30);
 $path = "/";
 $domain = ""; // your domain here for validation of the cookies
 $secure = false;
 $httponly = true;

 setcookie($cookie_name, $token, $expire, $path, $domain, $secure, $httponly );

 ?>
