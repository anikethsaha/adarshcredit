<?php

include_once "config.php";


if (isset($_SESSION['logged_in'])
    && ($_SESSION['logged_in']) == TRUE
    && isset($_SESSION['uID'])
    && isset($_SESSION['user_role'])
 ) {

        if((isset($_SESSION['user_role']) && $_SESSION['user_role'] == 1 )
            && $user['role_group'] == 1){

        }else{
            header("Location: ./403forbidden.php") or die("Forbidden");
            exit;
        }
    }else{
        header("Location: ./login.php") or die("Forbidden");
        exit;
    }
?>