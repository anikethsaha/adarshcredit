<?php

include_once "config.php";

include_once "validator.php";

include_once "generateToken.php";

require 'PHPMailer/PHPMailerAutoload.php';


$headers = apache_request_headers();


function sanitize( $conn,$data) {
    $data = trim($data);
    $data = htmlspecialchars($data);
    $data = mysqli_real_escape_string($conn,$data);
    return $data;
}


function isEmailSent($conn,$email,$domain,$token_length){
    // Create new instance of generator class.
    $generator = new RandomStringGenerator();
    $code = $generator->generate($token_length);
    $randomPassword = $generator->generate($token_length);



    $link = $domain."/reset_password.php?code=".$code;
    $tokenstmt = $conn->prepare("
    UPDATE users
    SET
        activation_code = '".$code."',
        temp_password = '".$randomPassword."'
    WHERE email = ?");
    $tokenstmt->bind_param("s", $email);
    if($tokenstmt->execute()){
        $sql = "SELECT * FROM users WHERE email='".$email."'";
        $query = mysqli_query($conn,$sql);
        $customerUser = mysqli_fetch_array($query);
        $recieverEmail = $customerUser['email'];
        $recieverUsername = $customerUser['username'];
        $recieverTemp_Password = $randomPassword;
        $mail = new PHPMailer;
        //$mail->SMTPDebug = ;                               // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->SMTPDebug = 4;
        $mail->Host = SMTP_HOST;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = SMTPUSERNAME;                 // SMTP username
        $mail->Password = SMTPPASS;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom(MAIL_FROM_ADDRESS, MAIL_FROM_NAME);
        $mail->addAddress($recieverEmail, $recieverUsername);     // Add a recipient

        $mail->addReplyTo(MAIL_REPLY_EMAIL, MAIL_REPLY_NAME);
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');
          // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'Change Your Password Here' ;
        $mail->Body    = '
        <p>
        Responding On Forgot Password Request
        <br/> Steps To Reset Password
        <h4> COPY THIS PASSWORD THE BELOW</h4>
        <br/>
        <h2> "'.$recieverTemp_Password.'" </h2>
        <br/> And Click <a href="'.$link.'">here</a> to reset password
        </p>

        ';
        $mail->AltBody = 'Thanks For Choosing Us';
        if(!$mail->send()) {
            die('Message could not be sent. Mailer Error: ');
        } else {
            // echo 'Message has been sent';
            return TRUE;
        }
        $tokenstmt->close();
    }else{
        return FALSE;
    }

}

if(!isset($_COOKIE['CSRF_TOKEN']) || !isset($_POST['_token'])) {
    die("Forbidden");
}else{
    if( $_COOKIE['CSRF_TOKEN'] == $_POST['_token'] ){
        $validator = new FormValidator();
        if(!isset($_POST['email'])){
            die("INVALID EMAIL PLEASE GIVE CORRECT EMAIL ADDRESS");
        }else{
            $email = $_POST['email'];
            $validator->validateItem($email, 'email');

            $email = $validator->sanatizeItem($email, 'email');
            $stmt = $conn->prepare(" SELECT isActive FROM users WHERE email = ? ");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($isActive);
            $stmt->fetch();
            if($isActive == 0){
                die("This Account is Still Not Activated. Please Wait As Our Team Will Activation Email Soon");
            }else{
                if(!isEmailSent($conn,$email,$domain,$token_length)){
                    die("FAILED...ERROR IN SENDING THE MAIL ..PLEASE TRY AGAIN");
                }else{
                    echo "SUCCESS";
                    header( "refresh:1;url=login.php" );
                }
            }

        }
    }else{
        die("Forbidden");
    }

}
?>