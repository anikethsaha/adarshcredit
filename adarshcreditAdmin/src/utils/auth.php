<?php


include_once "config.php";



if (isset($_SESSION['logged_in'])
    && ($_SESSION['logged_in']) == TRUE
    && isset($_SESSION['uID'])
    && isset($_SESSION['user_role'])
 ) {
    //valid user has logged-in to the website
    //Check for unauthorized use of user sessions
        $iprecreate = $_SERVER['REMOTE_ADDR'];
        $useragentrecreate = $_SERVER["HTTP_USER_AGENT"];
        $signaturerecreate = $_SESSION['signature'];

    //Extract original salt from authorized signature
        $saltrecreate = substr($signaturerecreate, 0, $length_salt);
    //Extract original hash from authorized signature
        $originalhash = substr($signaturerecreate, $length_salt, 40);
    //Re-create the hash based on the user IP and user agent
    //then check if it is authorized or not
        $hashrecreate = sha1($saltrecreate . $iprecreate . $useragentrecreate);
        if (!($hashrecreate == $originalhash)) {
    //Signature submitted by the user does not matched with the
    //authorized signature
    //This is unauthorized access
    //Block it

            header(sprintf("Location: %s", $forbidden_url)) or die("Forbidden");;
            exit;
        }
    //Session Lifetime control for inactivity
    //Credits: http://stackoverflow.com/questions/520237/how-do-i-expire-a-php-session-after-30-minutes
        if ((isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > $sessiontimeout))) {
            session_destroy();
            session_unset();
    //redirect the user back to login page for re-authentication
            header("Location: ./login.php") or die("Forbidden");;
        }

        $_SESSION['LAST_ACTIVITY'] = time();
        $sql = "SELECT * FROM users where id='".$_SESSION["uID"]."'";
        $query = mysqli_query($conn,$sql);
        $user = mysqli_fetch_array($query);
        $user['signature'] =  $_SESSION['signature'];

    }else{
        header("Location: /login.php") or die("Forbidden");;
        exit;
    }
?>