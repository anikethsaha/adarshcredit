<?php
include_once "config.php";
include_once "validator.php";

include_once "generateToken.php";

// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;

// require 'PHPMailer/src/Exception.php';
// require 'PHPMailer/src/PHPMailer.php';
// require 'PHPMailer/src/SMTP.php';

require 'PHPMailer/PHPMailerAutoload.php';



$headers = apache_request_headers();


function sanitize( $conn,$data) {
    $data = trim($data);
    $data = htmlspecialchars($data);
    $data = mysqli_real_escape_string($conn,$data);
    return $data;
}
function isEmailSent($conn,$uID,$domain,$token_length){
    // Create new instance of generator class.
    $generator = new RandomStringGenerator();
    $code = $generator->generate($token_length);
    $link = $domain."/reset_password.php?code=".$code;
    $tokenstmt = $conn->prepare("UPDATE users SET 	activation_code = '".$code."' WHERE id = ?");
    $tokenstmt->bind_param("i", $uID);
    if($tokenstmt->execute()){
        $sql = "SELECT * FROM users WHERE id='".$uID."'";
        $query = mysqli_query($conn,$sql);
        $customerUser = mysqli_fetch_array($query);
        $recieverEmail = $customerUser['email'];
        $recieverUsername = $customerUser['username'];
        $recieverTemp_Password = $customerUser['temp_password'];
        $mail = new PHPMailer;
        //$mail->SMTPDebug = ;                               // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->SMTPDebug = 4;
        $mail->Host = SMTP_HOST;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = SMTPUSERNAME;                 // SMTP username
        $mail->Password = SMTPPASS;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom(MAIL_FROM_ADDRESS, MAIL_FROM_NAME);
        $mail->addAddress($recieverEmail, $recieverUsername);     // Add a recipient

        $mail->addReplyTo(MAIL_REPLY_EMAIL, MAIL_REPLY_NAME);
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');
          // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'Please Activate Your Account';
        $mail->Body    = '
        <p>
        Thank you for your request.
        <br/> Your account has been activated kindly find your  password to verify your details
        <h4> COPY THIS PASSWORD THE BELOW</h4>
        <br/>
        <h2> "'.$recieverTemp_Password.'" </h2>
        <br/> And Click <a href="'.$link.'">here</a> to verify and given password
        </p>

        ';
        $mail->AltBody = 'Thanks For Choosing Us';
        if(!$mail->send()) {
            die('Message could not be sent. Mailer Error: ');
        } else {
            // echo 'Message has been sent';
            return TRUE;
        }
        $tokenstmt->close();
    }else{
        return FALSE;
    }

}

if(!isset($_COOKIE['CSRF_TOKEN']) || !isset($headers['X-CSRF-TOKEN'])) {
    die("Forbidden");
}else{
    if( $_COOKIE['CSRF_TOKEN'] == $headers['X-CSRF-TOKEN'] ){
        $validator = new FormValidator();
        if(!isset($_POST['id'])){
            die("INVALID USER ID");
        }else{
            $uID = $_POST['id'];
            $validator->validateItem($uID, 'number');
            $uID = $validator->sanatizeItem($uID, 'number');
            if(!isEmailSent($conn,$uID,$domain,$token_length)){
                die("FAILED...ERROR IN SENDING THE MAIL ..PLEASE TRY AGAIN");
            }else{
                $stmt = $conn->prepare("UPDATE users SET isActive = 1 WHERE id = ?");
                $stmt->bind_param("i", $uID);
                if($stmt->execute()){

                    $stmt->close();
                    echo "EMAIL SENT ! isActive Status = 1";
                }else{
                    die("SOME ERROR IN THE DATABASE OPERATION ! Please Refresh and try again");
                    $stmt->close();
                }
            }

        }
    }else{
        die("Forbidden");
    }
}



?>