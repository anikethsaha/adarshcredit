<?php


require('utils/config.php');
function sanitize($data) {
    $data = trim($data);
    $data = htmlspecialchars($data);
    return $data;
}
$signature = sanitize($_GET['signature']);
if ($signature === $_SESSION['signature']) {
//authenticated user request
    $_SESSION['logged_in'] = False;
    session_destroy();
    session_unset();
    echo 'You have successfully logout from the private website! Thank you.<br /><br />';
    header( "refresh:5;url=./login.php" );
} else {
//unauthorized logout.
die("not authorized");
    header("Location: ./login.php");
    exit;
}


?>