
<?php include "./utils/config.php" ?>
<?php include_once "./utils/csrf_token.php" ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AdarshCredit|Admin </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js?render=<?php echo  $sitekey; ?>'></script>
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form
            method="POST"
            action="<?php echo htmlentities("utils/regenerate_password.php"); ?>"
             id="login_form"   >
              <h1>Password Form</h1>
              <div>
                <input type="hidden" name="_token" value="<?php echo $token; ?>">
                <input
                  type="email"
                  class="form-control"
                  placeholder="Write Your Email To Send Confirmation Mail"
                  name="email"
                  required
                />
              </div>
              <div>
                <input
                    required
                    type="hidden"
                    id="g-recaptcha-response"
                    name="g-recaptcha-response"
                    class="form-control"
                    />
                </div>
              <div>
                <button class="btn btn-default submit" type="submit"  >Sent Confirmation Mail</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div>
                  <h1>AdarshCredit</h1>

                </div>
              </div>
            </form>
          </section>
        </div>


      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="../build/js/validator.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />

        <script>

        grecaptcha.ready(function() {
        grecaptcha.execute('<?php echo  $sitekey; ?>', {action: 'action_name'})
        .then(function(token) {
        // Verify the token on the server.
            document.getElementById('g-recaptcha-response').value = token
        });
        });
</script>

  </body>
</html>

