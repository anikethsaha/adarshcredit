<?php require_once "./utils/auth.php" ?>
<?php require_once "./utils/isAdmin.php" ?>
<?php include "header.php" ?>
<?php include "sidebar.php" ?>
<?php include "navbar.php" ?>
<style>

.mini-btn{
    background-color: rgb(0, 184, 139);
    background-image: radial-gradient(at left top, rgb(0, 213, 189) 0%, rgb(0, 184, 139) 100%);
}

</style>
            <div class="right_col" role="main">
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      List Of Users
                    </p>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Username</th>
                          <th>email</th>
                          <th>IP</th>
                          <th>Login Attempts</th>
                          <th>Created On</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                          <?php #endregion
                                $listOfUserSQL = "SELECT * FROM users WHERE isActive = 0";
                                $listOfUserQuery = mysqli_query($conn,$listOfUserSQL);


                                while ($r = mysqli_fetch_array($listOfUserQuery)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $r['id'];  ?></td>
                                        <td><?php echo $r['username']; ?></td>
                                        <td><?php echo $r['email']; ?></td>
                                        <td><?php echo $r['ip']; ?></td>
                                        <td><?php echo $r['loginattempt']; ?></td>
                                        <td><?php echo $r['created_on']; ?></td>
                                        <td>
                                            <button onclick="sendEmailOTP('<?php echo $r['id'];  ?>');" class="btn btn-success dropdown-toggle btn-xs" >
                                                Grant Access
                                            </button>

                                        </td>

                                    </tr>

                                    <?php

                                }

                            ?>

                        </tbody>
                    </table>
                </div>

</div>

















<?php include "footer.php" ?>