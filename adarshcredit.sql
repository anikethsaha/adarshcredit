-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2019 at 02:42 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adarshcredit`
--

-- --------------------------------------------------------

--
-- Table structure for table `ipcheck`
--

CREATE TABLE `ipcheck` (
  `id` int(11) NOT NULL,
  `failedattempts` int(20) NOT NULL,
  `loggedip` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ipcheck`
--

INSERT INTO `ipcheck` (`id`, `failedattempts`, `loggedip`) VALUES
(9, 0, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `temp_password` varchar(30) NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '0',
  `ip` varchar(32) NOT NULL,
  `created_on` varchar(30) NOT NULL,
  `activation_code` varchar(100) NOT NULL,
  `role_group` int(2) NOT NULL,
  `loginattempt` int(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `temp_password`, `isActive`, `ip`, `created_on`, `activation_code`, `role_group`, `loginattempt`) VALUES
(1, 'Admin', 'admin@admin', 'user@123', '', 1, '', '', '', 1, 0),
(5, 'easueasy', 'john@gamm.com', '$2y$11$PB3po8hiaMkYmeD3ma9myuv', '', 2, '::1', '01/14/2019 01:48:00 pm', 'W1zyy24TnhSB1kJLVIe43P3cKvEJydPr', 2, 0),
(6, 'easueasssy', 'ss@sss.com', '$2y$11$q7.O1zW1SP6qe46cIDRMqug', '', 1, '::1', '01/14/2019 02:06:13 pm', 'ZNkclPAj8AsMQ79Ullw1OOOk7yt2dcfH', 2, 0),
(7, 'john', 'john@gamm.com', '$2y$11$zMO900evSpG.e4d6UuH2p.l', '', 0, '::1', '01/14/2019 02:10:18 pm', '7TsToPoo2no3WYXBzQo07MNXqsafl7Ke', 2, 0),
(8, 'lol', 'lol@lol.com', '$2y$11$bVXlIHVv6ATg.XN5h1KGdOA', '', 0, '::1', '01/14/2019 02:14:28 pm', '', 2, 0),
(9, 'easueasy', 'asd@asd.com', '$2y$11$7hrNolR1q8QylYfI7yfMW.9', '', 0, '::1', '01/14/2019 03:48:28 pm', '', 2, 0),
(10, 'easueasy', 'asd@asd.com', '$2y$11$.etyjUUnIhYYBPjDA.hCR.z', '', 0, '::1', '01/14/2019 04:03:15 pm', '', 2, 0),
(11, 'haate', 'haate@hate.com', '$2y$11$tXP/uxG0spwJwJWaCXZ8zeT', '', 0, '::1', '01/14/2019 04:03:43 pm', '', 2, 0),
(12, 'anix', 'anix@anix.com', '$2y$11$GRLSvdcYApZbntmWNLUSAeo', '', 0, '::1', '01/14/2019 07:26:38 pm', '', 2, 5),
(13, 'nasa', 'nasa@nasa.com', '$2y$11$8Ti6qx6O.gKYaoGAVsyL7eg', '', 0, '::1', '01/14/2019 08:20:14 pm', '', 2, 5),
(14, 'close', 'close@close.com', '$2y$10$6mynYBtQcWwzhHW2GV2sve8', '', 0, '::1', '01/14/2019 08:26:48 pm', '', 2, 5),
(15, 'easueasy', 'easny@snsn.com', '$2y$10$KUltKNu9N1nYaaR6C3OwtuG', '', 0, '::1', '01/14/2019 08:46:23 pm', '', 2, 0),
(16, 'kala', 'kala@gmail.com', '$2y$10$QArlc8G3NohhKLAFB0z2T.l', '', 0, '::1', '01/15/2019 12:14:15 pm', '', 2, 0),
(17, 'kala', 'kala@gmail.com', '$2y$10$tgYITBV9NWBGBjGsIYyNj.v', '', 1, '::1', '01/15/2019 12:16:19 pm', 'iMLWDWQmP52ASQCtwN15TNtWc9bN9JbZ', 2, 0),
(19, 'anikethsah', 'anik220798@gmail.com', '$2y$10$HqpDhK9L4DDd24/phI/Jtu8', '3Pnrfr7JGrH7O2JLtG3q2ZFWNGKy67', 1, '::1', '01/15/2019 12:34:35 pm', 'XoIH9WOObLpbT8DCcIUKuUEhK0CnyyfX', 2, 0),
(20, 'karunya', 'anikethsaha@karunya.edu.in', 'asd', '', 1, '::1', '01/15/2019 01:46:46 pm', 'gRWpswjwNhIoni6Z6dI2FW09t9R1e5I2', 2, 0),
(21, 'as', 'ss@sss.com', '$2y$10$fz7k0PjpiJnt9QSu5g7QNu3', 'nt5t04M8xDmH18n1rsE8kSaOByMZHb', 0, '::1', '01/15/2019 01:48:29 pm', '', 2, 0),
(22, 'john', 'ss@sss.com', '$2y$10$uu7sUpWKa52yVApA6brPbeS', 'xQfgbCPdJDmzfoHtlOcl88IkpS46i4', 0, '::1', '01/15/2019 01:49:45 pm', '', 2, 0),
(23, 'a', 'ss@sss.com', '$2y$10$1RDPoL7oQaIQElTd0yLR5ui', '97J8OVMfGSNpl9wtoXXT3hU93H5kxh', 0, '::1', '01/15/2019 01:50:28 pm', '', 2, 0),
(24, 'john', 'ss@sss.com', '$2y$10$iK68i46J4oSZhOTneRZZCe2', 'F1eWjaRTkUEvXtIss9AmllBaFZ7EsE', 0, '::1', '01/15/2019 01:51:25 pm', '', 2, 0),
(25, 'john', 'ss@sss.com', '$2y$10$mR0.1Icqb9cUPXnON.ukCuV', '8nRd1YNPdKW4GnrFMN5Sg1hfYMU4PV', 0, '::1', '01/15/2019 01:53:40 pm', '', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL,
  `role_name` varchar(20) NOT NULL,
  `role_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `role_name`, `role_id`) VALUES
(1, 'ADMIN', 1),
(2, 'CUSTOMER', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ipcheck`
--
ALTER TABLE `ipcheck`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ipcheck`
--
ALTER TABLE `ipcheck`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
